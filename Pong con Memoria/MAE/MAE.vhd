----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 19:37:03
-- Design Name: 
-- Module Name: MAE - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.Pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MAE is
    Port ( clk : in STD_LOGIC;
           reset : in std_logic;
           dir : in STD_LOGIC_VECTOR (11 downto 0);
           data : out STD_LOGIC_VECTOR (0 downto 0));
end MAE;

architecture Behavioral of MAE is
signal rsta_busy : std_logic;
signal rstb_busy : std_logic;

signal we : std_logic_vector(0 downto 0);
signal ab : std_logic_vector(11 downto 0);
signal dbi : std_logic_vector(0 downto 0);
signal dbo : std_logic_vector(0 downto 0);

begin

c1 : mem port map
(clk,reset,we,ab,dbi,dbo,clk,"0",dir,"0",data,rsta_busy,rstb_busy);

end Behavioral;
