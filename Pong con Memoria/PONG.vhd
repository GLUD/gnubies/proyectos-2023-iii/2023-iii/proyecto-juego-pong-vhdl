----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 20:13:07
-- Design Name: 
-- Module Name: PONG - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.Pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PONG is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           rgb : out STD_LOGIC_VECTOR(11 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC);
end PONG;

architecture Behavioral of PONG is
signal clki : std_logic;
signal data : std_logic_vector(0 downto 0);
signal dir : std_logic_vector(11 downto 0);
begin
process(clk,reset)
    begin
        if reset = '1' then
            clki<='0';
        elsif clk'event and clk='1' then
            clki<=not clki;
        end if;
end process;

c1 : VGAPong port map
(clki,reset,dir,data,rgb,hsync,vsync);

c2 : MAE port map
(clki,reset,dir,data);

end Behavioral;
