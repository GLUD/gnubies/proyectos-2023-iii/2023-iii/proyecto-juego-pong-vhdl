----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 19:17:20
-- Design Name: 
-- Module Name: Pack - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


package Pack is

component VGAPong is
        Port (clk : in STD_LOGIC;
              reset : in STD_LOGIC;
              dir : out std_logic_vector(11 downto 0);
              data : in std_logic_vector(0 downto 0);
              rgb : out STD_LOGIC_VECTOR (11 downto 0);
              hsync : out STD_LOGIC;
              vsync : out STD_LOGIC);
end component;

component MAE is
    Port ( clk : in STD_LOGIC;
           reset : in std_logic;
           dir : in STD_LOGIC_VECTOR (11 downto 0);
           data : out STD_LOGIC_VECTOR (0 downto 0));
end component;

component GrU is
    Port ( hcount : in STD_LOGIC_VECTOR (10 downto 0);
           vcount : in STD_LOGIC_VECTOR (9 downto 0);
           data : in STD_LOGIC_VECTOR (0 downto 0);
           rgb : out STD_LOGIC_VECTOR (11 downto 0));
end component;

component VGACont is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           dir : out STD_LOGIC_VECTOR (11 downto 0);
           hcount : out STD_LOGIC_VECTOR (10 downto 0);
           vcount : out STD_LOGIC_VECTOR (9 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC);
end component;

component mem IS
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    clkb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rsta_busy : OUT STD_LOGIC;
    rstb_busy : OUT STD_LOGIC
  );
END component;



end Pack;
