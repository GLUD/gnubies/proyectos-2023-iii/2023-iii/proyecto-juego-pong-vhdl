----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 19:02:59
-- Design Name: 
-- Module Name: GrU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity GrU is
    Port ( hcount : in STD_LOGIC_VECTOR (10 downto 0);
           vcount : in STD_LOGIC_VECTOR (9 downto 0);
           data : in STD_LOGIC_VECTOR (0 downto 0);
           rgb : out STD_LOGIC_VECTOR (11 downto 0));
end GrU;

architecture Behavioral of GrU is

signal q 		: std_logic_vector ( 1 downto 0);
signal hs,hi 	: std_logic_vector ( 10 downto 0);
signal vs,vi 	: std_logic_vector (9 downto 0);

begin

process(hcount,hs,hi)
	begin
		if hcount >= hi and hcount <= hs then 
			q(0) <= '1';
		else
			q(0) <= '0';
		end if;
end process;

q(1) <= '1' when vcount >= vi and vcount <= vs else '0';

process(q,data)
	begin
		if data = "1" and q = "11" then 
			rgb <= ( others => '1'); --Aqu� se cambia el color
		else
			rgb <= ( others => '0');
		end if;
end process;

hs  <= "00"&hcount(8 downto 3)&"111";
hi <= "00"&hcount(8 downto 3)&"000";
vs <= "00"&vcount(7 downto 2)&"11";
vi <= "00"&vcount(7 downto 2)&"00";

end Behavioral;
