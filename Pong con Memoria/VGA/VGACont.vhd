----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 18:58:57
-- Design Name: 
-- Module Name: VGACont - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGACont is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           dir : out STD_LOGIC_VECTOR (11 downto 0);
           hcount : out STD_LOGIC_VECTOR (10 downto 0);
           vcount : out STD_LOGIC_VECTOR (9 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC);
end VGACont;

architecture Behavioral of VGACont is

signal h : std_logic_vector ( 10 downto 0);
signal v : std_logic_vector ( 9 downto 0);
signal enav : std_logic;

begin


hcount <= h;
 
vcount <= v;


dir <= v ( 7 downto 2)& h(8 downto 3);





vsync <= '0' when v > 493 and v < 496 else '1';

hsync <= '0' when h > 1320 and h < 1512 else '1';

process(clk,reset)
	begin
		if reset ='1' then 
			v <= ( others =>'0');
		elsif clk'event and clk = '1' then 
			if enav = '1' then 
				if v = 525 then 
					v <= ( others =>'0');
				else
					v <= v + 1;
				end if;
			else null;
			end if;
		end if;
end process;

process(clk,reset)
	begin
		if reset = '1' then 
			h <= ( others =>'0');
		elsif clk'event and clk ='1' then 
			if h = 1600 then 
				h <= ( others =>'0');
			else
				h <= h + 1;
			end if;
		end if;
end process;


enav <='1' when h = 1600 else '0';

end Behavioral;
