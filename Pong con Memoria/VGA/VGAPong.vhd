----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2023 20:20:12
-- Design Name: 
-- Module Name: VGAPong - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.Pack.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGAPong is
        Port (clk : in STD_LOGIC;
              reset : in STD_LOGIC;
              dir : out std_logic_vector(11 downto 0);
              data : in std_logic_vector(0 downto 0);
              rgb : out STD_LOGIC_VECTOR (11 downto 0);
              hsync : out STD_LOGIC;
              vsync : out STD_LOGIC);
end VGAPong;

architecture Behavioral of VGAPong is
signal hcount : STD_LOGIC_VECTOR (10 downto 0);
signal vcount : STD_LOGIC_VECTOR (9 downto 0);
begin

c1 : VGACont port map
(clk,reset,dir,hcount,vcount,hsync,vsync);


c2 : GrU port map
(hcount,vcount,data,rgb);

end Behavioral;
