----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 13:59:52
-- Design Name: 
-- Module Name: GrU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity GrU is
    Port ( hcount : in STD_LOGIC_VECTOR (9 downto 0);
           vcount : in STD_LOGIC_VECTOR (9 downto 0);
           rgb : out STD_LOGIC_VECTOR (11 downto 0);
           redi : in std_logic_vector(3 downto 0);
           greeni : in std_logic_vector(3 downto 0);
           bluei : in std_logic_vector(3 downto 0));
end GrU;

architecture Behavioral of GrU is
signal red : std_logic_vector(3 downto 0);
signal green : std_logic_vector(3 downto 0);
signal blue : std_logic_vector(3 downto 0);

begin

rgb<=red&green&blue;

--No dibuje en el porch
process(hcount,vcount)
begin
    if (hcount >= 0 and hcount <144) or (vcount >=0 and vcount <35) then
        red<=(others=>'0');
        green<=(others=>'0');
        blue<=(others=>'0');
    else
        --Dibuje un marco blanco de 10px de grosor
        if (hcount >= 144 and hcount <154) or (hcount >= 773 and hcount <=783) or (vcount >=35 and vcount <45) or (vcount >=504 and vcount <=514) then
            red<=(others=>'1');
            green<=(others=>'1');
            blue<=(others=>'1');
         else
            red<=redi;
            green<=greeni;
            blue<=bluei;
         end if;
     end if;
end process;

end Behavioral;
