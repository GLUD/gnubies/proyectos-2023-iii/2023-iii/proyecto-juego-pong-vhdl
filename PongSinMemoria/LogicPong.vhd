----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 23:41:22
-- Design Name: 
-- Module Name: LogicPong - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LogicPong is
    Port ( clk : in STD_LOGIC;
           reset : in std_logic;
           newframe : in std_logic;
           botonj1up : in std_logic;
           botonj2up : in std_logic;
           botonj1down : in std_logic;
           botonj2down : in std_logic;
           hcount : std_logic_vector(9 downto 0);
           vcount : std_logic_vector(9 downto 0);
           redi : out STD_LOGIC_VECTOR (3 downto 0);
           greeni : out STD_LOGIC_VECTOR (3 downto 0);
           bluei : out STD_LOGIC_VECTOR (3 downto 0));
end LogicPong;

architecture Behavioral of LogicPong is
--Constantes de Velocidad(se pueden cambiar)
constant velocidadBarra : integer :=10;
constant velocidadBola : integer :=6;

--Inicializaci�n de sa�ales de colores que se puedan leer, recuerda que no se puede leer salidas de un componente
signal redo : STD_LOGIC_VECTOR (3 downto 0):="0000";
signal greeno : STD_LOGIC_VECTOR (3 downto 0):="0000";
signal blueo : STD_LOGIC_VECTOR (3 downto 0):="0000";

--Rangos de dibujo de la barras
signal barrah1 : integer range 154 to 773:=164;
signal barrav1 : integer range 45 to 504:=274;
signal barrah2 : integer range 154 to 773:=748;
signal barrav2 : integer range 45 to 504:=274;

--Rangos de dibujo de la bola
signal bolah : integer range 154 to 773:=456;
signal bolav : integer range 45 to 504:=267;
signal bolabaja : std_logic:= '0';
signal bolacorrederecha : std_logic:='1';

signal timer : integer range 0 to 1000000;
begin
redi<=redo;
greeni<=greeno;
bluei<=blueo;
--Dibuje las barras de 15x80 px
process(clk)
begin
    if clk'event and clk='1' then
        if (hcount >= barrah1 and hcount < barrah1+15) and (vcount >= barrav1 and vcount < barrav1+80) then
            blueo<="1111";
         elsif (hcount >= barrah2 and hcount < barrah2+15) and (vcount >= barrav2 and vcount < barrav2+80) then
            blueo<="1111";
         else
            blueo<=(others=>'0');
         end if;
     end if;
end process;

--Dibuje la bola de 15x15 px
process(clk)
begin
    if clk'event and clk='1' then
        if (hcount >= bolah and hcount < bolah+15) and (vcount >= bolav and vcount < bolav+15) then
            greeno<="1111";
        else
            greeno<=(others=>'0');
        end if;
     end if;
end process;

--Mueva las barras
process(clk)
begin
    if clk'event and clk='1' and newframe='1' then
        if timer=1000000 then
    --Movimiento del jugador 2
        timer<=0;
        if botonj2down = '1' then
            if barrav2<424 then
                barrav2 <= barrav2 + velocidadBarra;
            else
                barrav2<=barrav2;
            end if;
        elsif botonj2up = '1' then
            if barrav2>45 then
                barrav2 <= barrav2 - velocidadBarra;
            else
                barrav2<=barrav2;
            end if;
        end if;
    --Movimiento del jugador 1
        if botonj1down = '1' then
            if barrav1<424 then
                barrav1 <= barrav1 + velocidadBarra;
            else
                barrav1<=barrav1;
            end if;
        elsif botonj1up = '1' then
            if barrav1>45 then
                barrav1 <= barrav1 - velocidadBarra;
            else
                barrav1<=barrav1;
            end if;
        end if;
    else
        timer<=timer+1;
    end if;
    end if;
end process;

--Mueva la bola
process(clk,blueo,greeno)
begin
    if clk'event and clk='1' and newframe='1' then
        -- pongame la bola en el lugar de inicio cuando haga reseteo del juego
        if reset='1' then
            bolah<=456;
            bolav<=267;
        else
            if timer = 1000000 then
                --la bola va para abajo pero no ha llegado al borde inferior
                if bolav<489 and bolabaja='1' then
                    bolav<=bolav + velocidadBola;
                elsif bolabaja='1' then --cuando toca el borde inferior se devuelve
                    bolabaja<='0';
                --la bola va para arriba pero no ha llegado al borde superior 
                elsif bolav>45 and bolabaja='0' then
                    bolav<=bolav - velocidadBola;
                elsif bolabaja='0' then --cuando toca el borde superior se devuelve
                    bolabaja<='1';
                end if;
                
                --la bola va para la derecha pero no ha llegado al borde derecho
                if bolah<758 and bolacorrederecha='1' then
                    bolah<=bolah + velocidadBola;
                elsif bolacorrederecha='1' then --cuando toca el borde derecho se devuelve
                    bolacorrederecha<='0';
                    bolah<=456;
                    bolav<=267;
                --la bola va para la izquierda pero no ha llegado al borde izquierdo
                elsif bolah>154 and bolacorrederecha='0' then
                    bolah<=bolah - velocidadBola;
                elsif bolacorrederecha='0' then --cuando toca el borde izquierdo se devuelve
                    bolacorrederecha<='1';
                    bolah<=456;
                    bolav<=267;
                end if;       
            else
                if blueo="1111" and greeno="1111" then
                    bolacorrederecha<=not bolacorrederecha;
                end if;
            end if;
        end if;
    end if;
end process;
        
            
            
         
end Behavioral;
