----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 11:55:56
-- Design Name: 
-- Module Name: PONG - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.Pack.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PONG is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           botonj1up : in std_logic;
           botonj2up : in std_logic;
           botonj1down : in std_logic;
           botonj2down : in std_logic;
           rgb : out STD_LOGIC_VECTOR (11 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC);
end PONG;

architecture Behavioral of PONG is
signal hcount : std_logic_vector(9 downto 0);
signal vcount : std_logic_vector(9 downto 0);
signal clk50MHz : std_logic;
signal clk25MHz : std_logic;
signal redi : std_logic_vector(3 downto 0);
signal greeni : std_logic_vector(3 downto 0);
signal bluei : std_logic_vector(3 downto 0);
signal newframe : std_logic;

begin

c1: VGACont port map
(clk=>clk25MHz,reset=>reset,hcount=>hcount,vcount=>vcount,hsync=>hsync,vsync=>vsync,newframe=>newframe);

c2 : GrU port map
(hcount=>hcount,vcount=>vcount,rgb=>rgb,redi=>redi,greeni=>greeni,bluei=>bluei);

c3 : LogicPong port map
(clk=>clk25MHz,reset=>reset,newframe=>newframe,botonj1up=>botonj1up,botonj2up=>botonj2up,botonj1down=>botonj1down,botonj2down=>botonj2down,hcount=>hcount,vcount=>vcount,redi=>redi,greeni=>greeni,bluei=>bluei);

process(clk,reset)
    begin
        if reset='1' then
            clk50Mhz<='0';
        elsif clk'event and clk='1' then
            clk50MHz<=not clk50MHz;
        end if;
end process;

process(clk50MHz,reset)
    begin
        if reset='1' then
            clk25MHz<='0';
        elsif clk50Mhz'event and clk50Mhz='1' then
            clk25MHz<=not clk25MHz;
        end if;
end process;

end Behavioral;
