----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 11:56:46
-- Design Name: 
-- Module Name: Pack - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package Pack is

component VGACont is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           hcount : out STD_LOGIC_VECTOR (9 downto 0);
           vcount : out STD_LOGIC_VECTOR (9 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC;
           newframe : out std_logic);
end component;

component GrU is
    Port ( hcount : in STD_LOGIC_VECTOR (9 downto 0);
           vcount : in STD_LOGIC_VECTOR (9 downto 0);
           rgb : out STD_LOGIC_VECTOR (11 downto 0);
           redi : in std_logic_vector(3 downto 0);
           greeni : in std_logic_vector(3 downto 0);
           bluei : in std_logic_vector(3 downto 0));
end component;

component LogicPong is
    Port ( clk : in STD_LOGIC;
           reset : in std_logic;
           newframe : in std_logic;
           botonj1up : in std_logic;
           botonj2up : in std_logic;
           botonj1down : in std_logic;
           botonj2down : in std_logic;
           hcount : std_logic_vector(9 downto 0);
           vcount : std_logic_vector(9 downto 0);
           redi : out STD_LOGIC_VECTOR (3 downto 0);
           greeni : out STD_LOGIC_VECTOR (3 downto 0);
           bluei : out STD_LOGIC_VECTOR (3 downto 0));
end component;

end Pack;

