----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 12:22:39
-- Design Name: 
-- Module Name: PruebaVGACont - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PruebaVGACont is
end PruebaVGACont;

architecture Behavioral of PruebaVGACont is

component PONG
    port(
        clk : in STD_LOGIC;
        reset : in STD_LOGIC;
        rgb : out STD_LOGIC_VECTOR (11 downto 0);
        hsync : out STD_LOGIC;
        vsync : out STD_LOGIC
        );
end component;

--Inputs
signal clk : std_logic :='0';
signal reset : std_logic :='0';

--Outputs
signal rgb : STD_LOGIC_VECTOR (11 downto 0);
signal hsync : STD_LOGIC;
signal vsync : STD_LOGIC;

-- Clock period definitions
constant clk_period : time :=10ns;

begin

-- Instantiate the Unit Under Test (UUT)
uut : PONG port map (
        clk => clk,
        reset => reset,
        rgb => rgb,
        hsync => hsync,
        vsync => vsync
    );

-- Clock process definitions
clk_process :process
begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
end process;

-- Stimulus process
stim_proc: process
begin
    reset<='1';
    wait for 20ns;
    reset<='0';
    wait;
end process;

end Behavioral;
