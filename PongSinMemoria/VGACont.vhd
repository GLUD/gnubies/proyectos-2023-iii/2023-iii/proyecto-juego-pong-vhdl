----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.11.2023 11:27:23
-- Design Name: 
-- Module Name: VGACont - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all; 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VGACont is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           hcount : out STD_LOGIC_VECTOR (9 downto 0);
           vcount : out STD_LOGIC_VECTOR (9 downto 0);
           hsync : out STD_LOGIC;
           vsync : out STD_LOGIC;
           newframe : out std_logic);
end VGACont;

architecture Behavioral of VGACont is
signal h : std_logic_vector(9 downto 0);
signal v : std_logic_vector(9 downto 0);

begin
hcount<=h;
vcount<=v;

process(clk,reset)
    begin
        if reset='1' then
            h<=(others=>'0');
        elsif clk'event and clk='1' then
            if h=799 then
                h<=(others=>'0');
            else
                h<=h+1;
                newframe<='0';
            end if;
        end if;
end process;

process(clk,reset,h)
    begin
        if reset='1' then
            v<=(others=>'0');
        elsif clk'event and clk='1' then
            if h=799 then
                if v=525 then
                    v<=(others=>'0');
                    newframe<='1';
                else
                    v<=v+1;
                end if;
             end if;
         end if;
end process;

hsync <='0' when h>=0 and h<96 else '1';
vsync <='0' when v>=0 and v<2 else '1';

end Behavioral;
